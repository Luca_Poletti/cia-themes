var map;
var markers = [];
var input = document.getElementById('locationTextField');
var latitudine = document.getElementById('latitudine');
var longitudine = document.getElementById('longitudine');

function initMap() {
    var italy = {
        lat: 41.9,
        lng: 12.4833333
    };

    //var input = document.getElementById('locationTextField');
    //var autocomplete = new google.maps.places.Autocomplete(input);
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        center: italy
    });
    map.addListener('click', function(event) {
        addMarker(event.latLng);
    });
}

function addMarker(location) {
    deleteMarkers();
    var marker = new google.maps.Marker({
        position: location,
        map: map
    });
    markers.push(marker);
    map.panTo(location);
    var latitudine = document.getElementById('latitudine');
    latitudine.value= marker.getPosition().lat();
    var longitudine = document.getElementById('longitudine');
    longitudine.value= marker.getPosition().lng();
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
    }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
    setMapOnAll(null);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
    clearMarkers();
    markers = [];
}

var updateAddress = {
    autocomplete: new google.maps.places.Autocomplete(input),
    event: function(){
        var self = this;
        google.maps.event.addListener(self.autocomplete, 'place_changed', function() {
            var place = self.autocomplete.getPlace(),
                address = place.address_components,
                streetAddress = '';

            for (var i = 0; i < address.length; i++) {
                var addressType = address[i].types[0];

                if (addressType == 'subpremise') {
                    streetAddress += address[i].long_name + '/';
                }
                if (addressType == 'street_number') {
                    streetAddress += address[i].long_name + ' ';
                }
                if (address[i].types[0] == 'route') {
                    streetAddress += address[i].long_name;
                }
            }

            // update the textboxes
            $('#locationTextField').val(streetAddress);
            $('#latitudine').val(place.geometry.location.lat);
            $('#longitudine').val(place.geometry.location.lng);
            addMarker(event.latLng)
        });

    }
};
updateAddress.event();

